package ioio.examples.hello;

import ioio.lib.api.DigitalInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.IOIO.VersionType;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.view.KeyEvent;
import android.view.View;


/**
 * This is the main activity of the HelloIOIO example application.
 *
 * It displays a toggle button on the screen, which enables control of the
 * on-board LED. This example shows a very simple usage of the IOIO, by using
 * the {@link IOIOActivity} class. For a more advanced use case, see the
 * HelloIOIOPower example.
 */
public class MainActivity extends IOIOActivity {
	DigitalInput joystick_up;
	DigitalInput joystick_down;
	DigitalInput joystick_left;
	DigitalInput joystick_right;
	DigitalInput joystick_fire;
    
    private ImageView up_arrow;
    private ImageView down_arrow;
    private ImageView left_arrow;
    private ImageView right_arrow;
    private ImageView fire_indicator;
	
	private ToggleButton button_;

	/**
	 * Called when the activity is first created. Here we normally initialize
	 * our GUI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		button_ = (ToggleButton) findViewById(R.id.button);
        up_arrow = (ImageView) findViewById(R.id.up_arrow);
        down_arrow = (ImageView) findViewById(R.id.down_arrow);
        left_arrow = (ImageView) findViewById(R.id.left_arrow);
        right_arrow = (ImageView) findViewById(R.id.right_arrow);
        fire_indicator = (ImageView) findViewById(R.id.fire_indicator);
        up_arrow.setVisibility(View.INVISIBLE);
        down_arrow.setVisibility(View.INVISIBLE);
        left_arrow.setVisibility(View.INVISIBLE);
        right_arrow.setVisibility(View.INVISIBLE);
        fire_indicator.setVisibility(View.INVISIBLE);
	}

	/**
	 * This is the thread on which all the IOIO activity happens. It will be run
	 * every time the application is resumed and aborted when it is paused. The
	 * method setup() will be called right after a connection with the IOIO has
	 * been established (which might happen several times!). Then, loop() will
	 * be called repetitively until the IOIO gets disconnected.
	 */
	class Looper extends BaseIOIOLooper {
		/** The on-board LED. */
		private DigitalOutput led_;
		private boolean up_val;
		private boolean old_up_val;
		private boolean down_val;
		private boolean old_down_val;
		private boolean left_val;
		private boolean old_left_val;
		private boolean right_val;
		private boolean old_right_val;
		private boolean fire_val;
		private boolean old_fire_val;
		
		/**
		 * Called every time a connection with IOIO has been established.
		 * Typically used to open pins.
		 *
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 *
		 * @see ioio.lib.util.IOIOLooper#setup()
		 */
		@Override
		protected void setup() throws ConnectionLostException {
			showVersions(ioio_, "IOIO connected!");
			led_ = ioio_.openDigitalOutput(0, true);
			joystick_up = ioio_.openDigitalInput(40);
			joystick_down = ioio_.openDigitalInput(42);
			joystick_left = ioio_.openDigitalInput(43);
			joystick_right = ioio_.openDigitalInput(41);
			joystick_fire = ioio_.openDigitalInput(45);
			up_val = false;
			old_up_val = false;
			down_val = false;
			old_down_val = false;
			left_val = false;
			old_left_val = false;
			right_val = false;
			old_right_val = false;
			fire_val = false;
			old_fire_val = false;
			enableUi(true);
		}

		/**
		 * Called repetitively while the IOIO is connected.
		 *
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * @throws InterruptedException
		 * 				When the IOIO thread has been interrupted.
		 *
		 * @see ioio.lib.util.IOIOLooper#loop()
		 */
		@Override
		public void loop() throws ConnectionLostException, InterruptedException {
			led_.write(!button_.isChecked());
			Thread.sleep(100);

			up_val = joystick_up.read(); 
			if (up_val == old_up_val) {
				// The button state is unchanged, do nothing.
			} else if (up_val) {
				// The up button has just been pressed anew.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_DPAD_UP));
				up_arrow.setVisibility(View.VISIBLE);
				old_up_val = up_val;
			} else {
				// The down button has just been released.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DPAD_UP));
				up_arrow.setVisibility(View.INVISIBLE);
				old_up_val = up_val;
			}

			down_val = joystick_down.read(); 
			if (down_val == old_down_val) {
				// The button state is unchanged, do nothing.
			} else if (down_val) {
				// The up button has just been pressed anew.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_DPAD_DOWN));
				down_arrow.setVisibility(View.VISIBLE);
				old_down_val = down_val;
			} else {
				// The down button has just been released.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DPAD_DOWN));
				down_arrow.setVisibility(View.INVISIBLE);
				old_down_val = down_val;
			}

			left_val = joystick_left.read(); 
			if (left_val == old_left_val) {
				// The button state is unchanged, do nothing.
			} else if (down_val) {
				// The up button has just been pressed anew.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_DPAD_LEFT));
				left_arrow.setVisibility(View.VISIBLE);
				old_left_val = left_val;
			} else {
				// The down button has just been released.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DPAD_LEFT));
				left_arrow.setVisibility(View.INVISIBLE);
				old_left_val = left_val;
			}

			right_val = joystick_right.read(); 
			if (right_val == old_right_val) {
				// The button state is unchanged, do nothing.
			} else if (down_val) {
				// The up button has just been pressed anew.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_DPAD_RIGHT));
				right_arrow.setVisibility(View.VISIBLE);
				old_right_val = right_val;
			} else {
				// The down button has just been released.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DPAD_RIGHT));
				right_arrow.setVisibility(View.INVISIBLE);
				old_right_val = right_val;
			}

			fire_val = joystick_fire.read(); 
			if (fire_val == old_fire_val) {
				// The button state is unchanged, do nothing.
			} else if (down_val) {
				// The up button has just been pressed anew.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_BUTTON_A));
				fire_indicator.setVisibility(View.VISIBLE);
				old_fire_val = fire_val;
			} else {
				// The down button has just been released.
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_BUTTON_A));
				fire_indicator.setVisibility(View.INVISIBLE);
				old_fire_val = fire_val;
			}

		}

		/**
		 * Called when the IOIO is disconnected.
		 *
		 * @see ioio.lib.util.IOIOLooper#disconnected()
		 */
		@Override
		public void disconnected() {
			enableUi(false);
			toast("IOIO disconnected");
		}

		/**
		 * Called when the IOIO is connected, but has an incompatible firmware version.
		 *
		 * @see ioio.lib.util.IOIOLooper#incompatible(IOIO)
		 */
		@Override
		public void incompatible() {
			showVersions(ioio_, "Incompatible firmware version!");
		}
}

	/**
	 * A method to create our IOIO thread.
	 *
	 * @see ioio.lib.util.AbstractIOIOActivity#createIOIOThread()
	 */
	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper();
	}

	private void showVersions(IOIO ioio, String title) {
		toast(String.format("%s\n" +
				"IOIOLib: %s\n" +
				"Application firmware: %s\n" +
				"Bootloader firmware: %s\n" +
				"Hardware: %s",
				title,
				ioio.getImplVersion(VersionType.IOIOLIB_VER),
				ioio.getImplVersion(VersionType.APP_FIRMWARE_VER),
				ioio.getImplVersion(VersionType.BOOTLOADER_VER),
				ioio.getImplVersion(VersionType.HARDWARE_VER)));
	}

	private void toast(final String message) {
		final Context context = this;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		});
	}

	private int numConnected_ = 0;

	private void enableUi(final boolean enable) {
		// This is slightly trickier than expected to support a multi-IOIO use-case.
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (enable) {
					if (numConnected_++ == 0) {
						button_.setEnabled(true);
					}
				} else {
					if (--numConnected_ == 0) {
						button_.setEnabled(false);
					}
				}
			}
		});
	}
}